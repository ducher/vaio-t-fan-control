#Vaio T Fan Control

This piece of code lets you control the fan of your annoyingly noisy Vaio T11 
or T13 (and maybe T15 and also E serie).

#Usage

Compile it (gcc vaio-t-fancontrol.c -o vaio-t-fancontrol) and run it as root.
The options are:

 - -b to restore the fan speed to the factory method (controled by BIOS)
 - -a to have the fan speed automaticly controled by this software (experimental)
 - -r to read the sensors values (now just the temperature)
 - -s to set the fan speed to a specific value (Max speed ~70, min 255(dangerous))

#Credits

By Pierre Ducher (pierre.ducher gmail.com)


Reused some piece of code by Jonas Diemer (diemer@gmx.de) for the EC Command Program Sequence,
he's himself thanking Tord Lindstrom (pukko@home.se) for LGXfan and has used section 
"4.10.1.4 EC Command Program Sequence" in http://wiki.laptop.org/images/a/ab/KB3700-ds-01.pdf.

#Warning

This is experimental and a work in progress, use it a your own risk.
The values of the steps may be too low and cause overheating.
