/*
	More than simple fan controlling for my sony vaio T11.
	by Pierre Ducher pierre.ducher gmail.com
	
	This should also work for the vaio T13 and T15, and the E line as they have
	the same register adresses to control the fan but I cannot guarantee it.
	This is experimental and a work in progress, use it a your own risk.
	The values of the steps may be too low and cause overheating.
	
	To test it, simply compile it with gcc and execute it.
	The options are: -r to read the temperature, -a to set automode, -s to set 
	speed manually (Max speed ~70, min 255(dangerous)), -b to set to automatic 
	bios controled fan mode.

	Thanks: Jonas Diemer (diemer@gmx.de) for having done a similar program for 
	the MSI Wind, so I could understand how EC Command Program Sequence works.

*/
#include <sys/types.h>
#include <stdbool.h>
#include <stdint.h>
#include <unistd.h>
#include <stdio.h>
#include <sys/stat.h>
#include <fcntl.h>
// for inb outb
#include <sys/io.h>

#define EC_SC 0x66
#define EC_DATA 0x62

#define IBF 1
#define OBF 0
#define EC_SC_READ_CMD 0x80
#define EC_SC_WRITE_CMD 0x81
#define EC_SC_SCI_CMD 0x84

// EC addresses
#define EC_CUR_FAN_SPEED 0x95
#define EC_SET_FAN_SPEED 0x94
#define EC_CONF_REG 0x93
#define EC_FAN_SPEED 0x72
#define EC_TEMP_REG 0x51

// Values for the Vaio T
#define MANUAL_MODE 0x14
// auto bios mode
#define AUTO_MODE 0x4

#define POLL_INTERVAL 1
#define NB_STEP 3

// to store the command line parameters
struct parameters
{
	uint8_t fan_speed;
	bool set_speed;
	bool auto_mode;
	bool bios_mode;
	bool read_infos;
};

typedef struct step
{
	uint8_t up;
	uint8_t down;
	uint8_t speed;
} step;

struct parameters params = { 0x80, false, false, false};
bool is_auto = true;

step steps[NB_STEP];
uint8_t cur_step;

static int wait_ec(const uint32_t port, const uint32_t flag, const char value)
{
	uint8_t data;
	int i;

	i = 0;
	data = inb(port);

	while ( (((data >> flag)&0x1)!=value) && (i++ < 100)) {
		usleep(1000);
		data = inb(port);
	}
	if (i >= 100)
	{
		printf("wait_ec error on port 0x%x, data=0x%x, flag=0x%x, value=0x%x\n", port, data, flag, value);
		return 1;
	}

	return 0;
}

// For read_ec & write_ec command sequence see
// section "4.10.1.4 EC Command Program Sequence" in
// http://wiki.laptop.org/images/a/ab/KB3700-ds-01.pdf 
static uint8_t read_ec(const uint32_t port)
{
	uint8_t value;

	wait_ec(EC_SC, IBF, 0);
	outb(EC_SC_READ_CMD, EC_SC);
	wait_ec(EC_SC, IBF, 0);
	outb(port, EC_DATA);
	wait_ec(EC_SC, OBF, 1);
	value = inb(EC_DATA);

	return value;
}

static void write_ec(const uint32_t port, const uint8_t value)
{
	wait_ec(EC_SC, IBF, 0);
	outb(EC_SC_WRITE_CMD, EC_SC);
	wait_ec(EC_SC, IBF, 0);
	outb(port, EC_DATA);
	wait_ec(EC_SC, IBF, 0);
	outb(value, EC_DATA);
	wait_ec(EC_SC, IBF, 0);

	return;
}

static void set_auto ()
{
		// switching to auto mode
	write_ec(EC_CONF_REG, AUTO_MODE);
	is_auto = true;
}

static void set_speed(uint8_t speed)
{
	printf("Changing speed to %d...\n", speed);
	if (is_auto)
	{
		// switching to manual mode
		write_ec(EC_CONF_REG, MANUAL_MODE);
		is_auto = false;
	}
	// writing fan speed
	write_ec(EC_SET_FAN_SPEED, speed);
}

static int temperature()
{
	static int fd1 = -1;
	int temp = -1;
	
	if(fd1 == -1)
	{
		printf("Opening temperature file\n");
		fd1 = open("/sys/devices/platform/coretemp.0/temp1_input",O_RDONLY|O_NDELAY);
	}
	
	if (fd1 != -1)
	{
		char temp_string[16];
		sleep(POLL_INTERVAL);
		//printf("Prout\n");
		pread(fd1, temp_string, 8, 0x00);
		sscanf(temp_string, "%d", &temp);
		temp = temp/1000;
	}
	return temp;
}

static bool parse_args(int argc, char* argv[])
{
	char c;
	
	while((c = getopt(argc, argv, "abrs:")) != -1)
	{
		switch (c)
		{
			case 'a':
				params.auto_mode = true;
				break;
			case 'b':
				params.bios_mode = true;
				break;
			case 's':
				params.fan_speed = atoi(optarg);
				params.set_speed = true;
				break;
			case 'r':
				params.read_infos = true;
				break;
			case '?':
				printf("Usage: -a for auto -s <speed> for fan speed");
				return false;
			default:
				abort();
		}
	}
	return true;
}

int main(int argc, char *argv[])
{
	int fd1=-1;
	FILE *f;
	int iores;
	uint8_t fan_speed=0x00;
	int temp;
	char temp_string[16];

  // new method
	//bResult = InitializeWinIo();
		iores = ioperm(0x62,1,1);
		iores += ioperm(0x66,1,1);

	parse_args(argc, argv);
	if (!parse_args)
	{
		printf("Error parsing args\n");
		return 1;
	}
	printf("Auto : %u, Bios : %u, speed: %u\n", params.auto_mode, params.bios_mode, params.fan_speed);

	//init steps
	steps[0].up = 54;
	steps[1].up = 57;
	steps[2].up = 70;
	
	steps[0].down = 50;
	steps[1].down = 53;
	steps[2].down = 56;
	
	steps[0].speed = 140;
	steps[1].speed = 125;
	steps[2].speed = 110;
	
	cur_step = 0;

	if (iores == 0)
	{
    
    if (params.auto_mode)
    {
			fan_speed = read_ec(EC_CUR_FAN_SPEED);
			printf("Current fan speed : %u \n", fan_speed);
			set_speed(steps[cur_step].speed);
			
			for(;;)
			{
				temp = temperature();
				
				if(params.read_infos)
				{
					// rewrite the line
					printf("\rTemperature: %d ", temp);
					fflush(stdout);
				}
				// main engine!
				if(temp > steps[cur_step].up)
				{
					if (cur_step < NB_STEP - 1)
					{
						cur_step++;
						set_speed(steps[cur_step].speed);
					}
				}
				else if (temp < steps[cur_step].down)
				{
					if (cur_step > 0)
					{
						cur_step--;
						set_speed(steps[cur_step].speed);
					}
				}
			}
    }
    
    if (params.bios_mode)
    {
    	set_auto();
    }
    else if (params.set_speed)
    {
    	set_speed(params.fan_speed);
    }
    
    if (params.read_infos)
    {
    	for(;;)
    	{
    		temp = temperature();
    		printf("\rTemperature: %d ", temp);
				fflush(stdout);
    	}
    }
  }
  	else
	{
		printf("ioperm() failed!\n");
		exit(1);
	}


    return 0;
}
